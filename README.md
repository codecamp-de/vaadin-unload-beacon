# Vaadin UI Unload Beacon

This add-on uses the [Beacon API](https://developer.mozilla.org/en-US/docs/Web/API/Beacon_API) to signal 
whenever a page is [unloaded](https://developer.mozilla.org/en-US/docs/Web/API/Window/unload_event) in the 
browser in order to immediately close the UI on the server. This allows for much earlier clean up of UIs 
and associated resources than having to wait for missed heart beats or session timeouts to trigger a UI's
eventual disposal. Of course this is only on a best effort basis as there will always be situations where 
the browser is simply "gone" and no beacon will or can be sent.

This add-on in based on a technique demonstrated in the [Vaadin Cookbook](https://cookbook.vaadin.com/notice-closed).


## Getting started

In a Spring Boot application this add-on will work automatically once it has been added as dependency.
For other frameworks see [VaadinServiceInitListener](https://vaadin.com/docs/current/flow/advanced/tutorial-service-init-listener.html) 
on how to register the `UnloadBeaconServiceInitListener` manually.
