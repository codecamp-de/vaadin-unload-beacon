package de.codecamp.vaadin.unloadbeacon;


import com.vaadin.flow.component.ComponentEventListener;
import com.vaadin.flow.component.ComponentUtil;
import com.vaadin.flow.component.UI;
import com.vaadin.flow.shared.Registration;


public final class UnloadBeacon
{

  private UnloadBeacon()
  {
    // utility class
  }


  /**
   * Registers an unload beacon for the given UI.
   * <p>
   * Use the {@link UnloadBeaconServiceInitListener} to have this method automatically called for
   * all new UIs.
   *
   * @param ui
   *          the UI for which to register an unload beacon
   * @see UnloadBeaconServiceInitListener
   */
  public static void closeOnUnload(UI ui)
  {
    addUnloadListener(ui, beaconEvent ->
    {
      beaconEvent.getSource().close();
    });
  }

  /**
   * Registers a listener that will be notified when the unload beacon for the given UI is received.
   *
   * @param ui
   *          the UI to be watched for the unload event
   * @param listener
   *          the unload listener
   * @return the listener registration
   */
  public static Registration addUnloadListener(UI ui, ComponentEventListener<UnloadEvent> listener)
  {
    UnloadBeaconRequestHandler.install(ui);
    return ComponentUtil.addListener(ui, UnloadEvent.class, listener);
  }

}
