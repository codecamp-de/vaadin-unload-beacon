package de.codecamp.vaadin.unloadbeacon;


import com.vaadin.flow.component.ComponentUtil;
import com.vaadin.flow.component.UI;
import com.vaadin.flow.server.RequestHandler;
import com.vaadin.flow.server.SynchronizedRequestHandler;
import com.vaadin.flow.server.VaadinRequest;
import com.vaadin.flow.server.VaadinResponse;
import com.vaadin.flow.server.VaadinSession;
import java.io.IOException;
import java.util.UUID;


/**
 * A {@link RequestHandler} to receive the unload beacon.
 */
public class UnloadBeaconRequestHandler
  extends
    SynchronizedRequestHandler
{

  private final UI ui;

  private final String beaconPath;


  public UnloadBeaconRequestHandler(UI ui)
  {
    this.ui = ui;
    this.beaconPath = "/UNLOAD-BEACON/" + UUID.randomUUID().toString();
  }


  public UI getUi()
  {
    return ui;
  }

  public String getBeaconPath()
  {
    return beaconPath;
  }


  @Override
  protected boolean canHandleRequest(VaadinRequest request)
  {
    return beaconPath.equals(request.getPathInfo());
  }

  @Override
  public boolean synchronizedHandleRequest(VaadinSession session, VaadinRequest request,
      VaadinResponse response)
    throws IOException
  {
    ComponentUtil.fireEvent(ui, new UnloadEvent(ui));
    return true;
  }


  /**
   * Installs a {@link UnloadBeaconRequestHandler} for the given UI and registers the beacon on page
   * unload. If the beacon is already installed, this method does nothing.
   *
   * @param ui
   *          the UI for which to install the unload beacon
   */
  public static void install(UI ui)
  {
    if (ComponentUtil.getData(ui, UnloadBeaconRequestHandler.class) != null)
      return;

    UnloadBeaconRequestHandler handler = new UnloadBeaconRequestHandler(ui);

    // remove leading slash
    String beaconPath = handler.getBeaconPath().substring(1);

    ui.getElement().executeJs(
        "window.addEventListener('unload', function() { if (navigator.sendBeacon) navigator.sendBeacon($0) })",
        beaconPath);

    ui.getSession().addRequestHandler(handler);
    ui.addDetachListener(detachEvent ->
    {
      detachEvent.getSession().removeRequestHandler(handler);
    });

    ComponentUtil.setData(ui, UnloadBeaconRequestHandler.class, handler);
  }

}
