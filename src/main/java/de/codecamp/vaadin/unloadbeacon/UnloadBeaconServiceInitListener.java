package de.codecamp.vaadin.unloadbeacon;


import com.vaadin.flow.server.ServiceInitEvent;
import com.vaadin.flow.server.VaadinServiceInitListener;


/**
 * Registers an unload beacon for every newly created UI.
 */
public class UnloadBeaconServiceInitListener
  implements
    VaadinServiceInitListener
{

  @Override
  public void serviceInit(ServiceInitEvent serviceInitEvent)
  {
    serviceInitEvent.getSource().addUIInitListener(uiInitEvent ->
    {
      UnloadBeacon.closeOnUnload(uiInitEvent.getUI());
    });
  }

}
