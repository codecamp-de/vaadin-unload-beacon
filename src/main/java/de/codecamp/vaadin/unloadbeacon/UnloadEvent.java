package de.codecamp.vaadin.unloadbeacon;


import com.vaadin.flow.component.ComponentEvent;
import com.vaadin.flow.component.UI;


/**
 * This event is fired after a beacon has been received from the browser when a page has been
 * unloaded.
 */
public class UnloadEvent
  extends
    ComponentEvent<UI>
{

  public UnloadEvent(UI source)
  {
    super(source, true);
  }

}
