package de.codecamp.vaadin.unloadbeacon.autoconfigure;


import de.codecamp.vaadin.unloadbeacon.UnloadBeaconServiceInitListener;
import org.springframework.boot.autoconfigure.AutoConfiguration;
import org.springframework.context.annotation.Bean;


@AutoConfiguration
public class UnloadBeaconAutoConfiguration
{

  @Bean
  UnloadBeaconServiceInitListener vaadinUnloadBeaconServiceInitListener()
  {
    return new UnloadBeaconServiceInitListener();
  }

}
